﻿using Smartphone.Utility;

namespace Smartphone.Models
{
  public class Chassis
  {
    /// <summary>
    /// The material type of the chassis.
    /// </summary>
    public Lookups.MaterialTypes MaterialType {get; set;}
    /// <summary>
    /// The lenght of the chassis of the phone in inches.
    /// </summary>
    public decimal ChassisDepth {get; set;}

    /// <summary>
    /// The width of the chassis of the phone in inches.
    /// </summary>
    public decimal ChassisWidth {get; set;}

    /// <summary>
    /// The height of the chassis of the phone in inches.
    /// </summary>
    public decimal ChassisHeight {get; set;}

  }
}
