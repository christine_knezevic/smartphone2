﻿using Smartphone.Utility;

namespace Smartphone.Models
{
  public class App
  {
    public App(string name)
      {
        Name = name;
      }
    public string Name {get; private set;}
  }
}
