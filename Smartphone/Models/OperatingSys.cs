﻿
namespace Smartphone.Models
{
  public class OperatingSys
  {
    /// <summary>
    /// Set's the name for the operating system.
    /// </summary>
    public string Name {get; set;}
    
    /// <summary>
    /// Set's the version of the operating system.
    /// </summary>
    public string Version {get; set;}
  }
}
