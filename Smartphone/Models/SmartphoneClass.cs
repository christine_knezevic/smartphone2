﻿using System.Collections.Generic;
using System.Text;

namespace Smartphone.Models
{
  /// <summary>
  /// our sweet smartphone class
  /// </summary>
  public class SmartphoneClass
  {
    /// <summary>
    /// Smartphone's RAM property.
    /// </summary>
    public Ram Ram { get; set; }

    /// <summary>
    /// Smartphone's Screen Property.
    /// </summary>
    public Screen Screen { get; set; }

    /// <summary>
    /// Smartphone's Chassis property.
    /// </summary>
    public Chassis Chassis { get; set; }

    /// <summary>
    /// Smartphone's CPU property.
    /// </summary>
    public CentralProcessingUnit CentralProcessingUnit { get; set; }

    /// <summary>
    /// Smartphone's front camera properties.
    /// </summary>
    public Camera FrontCamera { get; set; }

    /// <summary>
    /// Smartphone's back camera properties.
    /// </summary>
    public Camera BackCamera { get; set; }

    /// <summary>
    /// Smartphone's operating system properties.
    /// </summary>
    public OperatingSys OperatingSys { get; set; }

    /// <summary>
    /// Smartphone's radio feature properties.
    /// </summary>
    public RadioFeatures RadioFeatures { get; set; }

    /// <summary>
    /// Smartphone's apps properties.
    /// </summary>
    public List<App> Apps { get; set; }
    
    /// <summary>
    /// Smartphone's battery properties.
    /// </summary>
    public Battery Battery { get; set; }
    
    /// <summary>
    /// The make of the smartphone.
    /// </summary>
    public string Make { get; set; }

    /// <summary>
    /// The model of the smartphone.
    /// </summary>
    public string Model { get; set; }

    /// <summary>
    /// The weight of the smartphone in ounces.
    /// </summary>
    public decimal Weight {get; set;}

    /// <summary>
    /// Tells whether the smartphone is water resistant.
    /// </summary>
    public bool IsWaterResistant {get; set;}

    public override string ToString()
    {
      var sb = new StringBuilder();

      sb.AppendFormat("Make: {0}", this.Make);
      sb.AppendFormat("\r\nModel: {0}", this.Model);
      sb.AppendFormat("\r\nRam: {0}", this.Ram.Size);
      
      return sb.ToString();
    }
  }
}
