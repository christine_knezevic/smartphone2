﻿
namespace Smartphone.Models
{
  public class CentralProcessingUnit
  {
    /// <summary>
    /// Sets the CPU make.
    /// </summary>
    public string CpuMake {get; set;}

    /// <summary>
    /// Sets the CPU model.
    /// </summary>
    public string CpuModel {get; set;}

    /// <summary>
    /// Sets the CPU Clock Speed.
    /// </summary>
    public decimal CpuClockSpeed {get; set;}
  }
}
