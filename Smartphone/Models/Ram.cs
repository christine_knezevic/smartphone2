﻿
namespace Smartphone.Models
{
  public class Ram
  {
    /// <summary>
    /// RAM size in megabytes
    /// </summary>
    public decimal Size { get; set; }
  }
}
