﻿
namespace Smartphone.Models
{
  public class RadioFeatures
  {
    /// <summary>
    /// Tells whether or not the smartphone has a GPS.
    /// </summary>
    public bool HasGPS {get; set;}

    /// <summary>
    /// Tells whether or not the smartphone has WIFI.
    /// </summary>
    public bool HasWifi {get; set;}

    /// <summary>
    /// Tells whether or not the smartphone has LTE.
    /// </summary>
    public bool HasLte {get; set;}


    /// <summary>
    /// Tells whether or not the smartphone has CDMA.
    /// </summary>
    public bool HasCdma {get; set;}
  }
}
