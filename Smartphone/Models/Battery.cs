﻿
namespace Smartphone.Models
{
  public class Battery
  {
    /// <summary>
    /// Battery kilowatts
    /// </summary>
    public int Kwh {get; set;}

    /// <summary>
    /// Tells if the battery is removable.
    /// </summary>
    public bool IsRemovable {get; set;}

    /// <summary>
    /// Tells what the max talk time is.
    /// </summary>
    public int MaxTalkTime {get; set;}
  }
}
