﻿
namespace Smartphone.Models
{
  public class Camera
  {
    /// <summary>
    /// Tells whether camera has a flash.
    /// </summary>
    public bool HasFlash {get; set;}

    /// <summary>
    /// Camera's resolution width.
    /// </summary>
    public int ResolutionWidth {get; set;}

    /// <summary>
    /// Camera's resolution height.
    /// </summary>
    public int ResolutionHeight {get; set;}

  }
}
