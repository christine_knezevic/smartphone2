﻿using Smartphone.Utility;

namespace Smartphone.Models
{
  public class Screen
  {
    /// <summary>
    /// Type of screen.
    /// </summary>
    public Lookups.ScreenTypes ScreenType { get; set; }

    /// <summary>
    /// Screen resolution width.
    /// </summary>
    public decimal ScreenWidth {get; set;}

    /// <summary>
    /// Screen resolution height.
    /// </summary>
    public decimal ScreenHeight {get; set;}

    /// <summary>
    /// Screen pixels per inch.
    /// </summary>
    public int PixelsPerInch {get; set;}
    
    /// <summary>
    /// True if screen is a touch screen
    /// </summary>
    public bool IsTouchScreen { get; set; }
  }
}
