﻿using Smartphone.Models;
using System;
using Smartphone.Utility;
using System.Collections.Generic;

namespace Smartphone
{
  class Program
  {
    static void Main(string[] args)
    {
      //this is where your program starts
      //Entry Point

      //instance of smartphone class
      var sc = new SmartphoneClass();

      //set all the properties
      sc.Ram = new Ram();
      sc.Ram.Size = 2048;

      sc.Screen = new Screen();
      sc.Screen.ScreenHeight = (decimal)5.41;
      sc.Screen.ScreenWidth = (decimal)2.69;
      sc.Screen.PixelsPerInch = 468;
      sc.Screen.IsTouchScreen = true;

      sc.Chassis = new Chassis();
      sc.Chassis.MaterialType = Lookups.MaterialTypes.Plastic;
      sc.Chassis.ChassisHeight = (decimal)5.41;
      sc.Chassis.ChassisWidth = (decimal)2.69;
      sc.Chassis.ChassisDepth = (decimal)0.37;

      sc.CentralProcessingUnit = new CentralProcessingUnit();
      sc.CentralProcessingUnit.CpuMake = "Qualcomm";
      sc.CentralProcessingUnit.CpuModel = "Snapdragon 600, quad-core";
      sc.CentralProcessingUnit.CpuClockSpeed = (decimal)1.7;
      
      sc.FrontCamera = new Camera();
      sc.FrontCamera.ResolutionHeight = 1920;
      sc.FrontCamera.ResolutionWidth = 1080;
      sc.FrontCamera.HasFlash = false;

      sc.BackCamera = new Camera();
      sc.BackCamera.ResolutionHeight = 1920;
      sc.BackCamera.ResolutionWidth = 1080;
      sc.BackCamera.HasFlash = true;

      sc.OperatingSys = new OperatingSys();
      sc.OperatingSys.Name = "Android";
      sc.OperatingSys.Version = "Sense 5.0 UI";

      sc.RadioFeatures = new RadioFeatures();
      sc.RadioFeatures.HasCdma = true;
      sc.RadioFeatures.HasGPS = true;
      sc.RadioFeatures.HasWifi = true;
      sc.RadioFeatures.HasLte = false;

      var apps = new List<App>();
      apps.Add(new App("Angry Birds"));
      apps.Add(new App("CamScanner"));
      sc.Apps = apps;

      sc.Battery = new Battery();
      sc.Battery.Kwh = 2300;
      sc.Battery.IsRemovable = false;
      sc.Battery.MaxTalkTime = 18;

      sc.Make = "HTC";

      sc.Model = "One";

      sc.Weight = (decimal)5.04;

      sc.IsWaterResistant = false;

      //write the phone details out to the console
      Console.WriteLine("**** My Smartphone ****");
      Console.WriteLine();
      Console.WriteLine("Make: {0}, Model: {1}", sc.Make, sc.Model);
      Console.WriteLine("Screen Size: {0} in. x {1} in.", sc.Screen.ScreenHeight, sc.Screen.ScreenWidth);
      Console.WriteLine("Screen Resolution: {0} pixels per inch.", sc.Screen.PixelsPerInch);
      Console.WriteLine("The phone {0} have a touchscreen.", (sc.Screen.IsTouchScreen) ? "does" : "does not");
      Console.WriteLine("Body is made from: {0}.",sc.Chassis.MaterialType);
      Console.WriteLine("Body is {0} x {1} x {2} inches.", sc.Chassis.ChassisHeight, 
          sc.Chassis.ChassisWidth, sc.Chassis.ChassisDepth);
      Console.WriteLine("Ram is {0}.", sc.Ram.Size);
      Console.WriteLine("Central Processing Unit is made by: {0}.", sc.CentralProcessingUnit.CpuMake);
      Console.WriteLine("Central Processing Unit model is: {0}.", sc.CentralProcessingUnit.CpuModel);
      Console.WriteLine("Central Processing Unit clock speed is: {0}", sc.CentralProcessingUnit.CpuClockSpeed);
      Console.WriteLine("Front Camera Resolution is {0} x {1} and has {2}.",
        sc.FrontCamera.ResolutionHeight, sc.FrontCamera.ResolutionWidth, (sc.FrontCamera.HasFlash) ? "flash" : "no flash");    
      Console.WriteLine("Back Camera Resolution is {0} x {1} and has {2}.",
            sc.BackCamera.ResolutionHeight, sc.BackCamera.ResolutionWidth, (sc.BackCamera.HasFlash) ? "flash" : "no flash");
      Console.WriteLine("The system has a {0} operating system, version: {1}.", sc.OperatingSys.Name,
          sc.OperatingSys.Version);
      Console.WriteLine("Radio Features include: \r\n CDMA: {0} \r\n GPS: {1} \r\n WIFI: {2} \r\n LTE: {3}",
        (sc.RadioFeatures.HasCdma) ? "Yes" : "No", (sc.RadioFeatures.HasGPS) ? "Yes" : "No",
        (sc.RadioFeatures.HasWifi) ? "Yes" : "No", (sc.RadioFeatures.HasLte) ? "Yes" : "No");
      Console.WriteLine("The system has the following apps:");
        foreach (var a in sc.Apps)
         {
           Console.WriteLine("\t{0}\t", a.Name); 
          }
      Console.WriteLine("The battery has {0} Kilowatts.", sc.Battery.Kwh);
      Console.WriteLine("The battery is {0}.", (sc.Battery.IsRemovable) ? "removable" : "not removable");
      Console.WriteLine("The battery's max talk time is: {0} hours.", sc.Battery.MaxTalkTime);
      Console.WriteLine("The phone is {0}.", (sc.IsWaterResistant) ? "water resistant" : "not water resistant");  
      Console.WriteLine();
      Console.WriteLine("**** Press the AnyKey ****");
      Console.ReadKey();

      //writing the same thing with the string builder
      Console.WriteLine(sc.ToString());
      Console.ReadKey();
    }
  }
}
